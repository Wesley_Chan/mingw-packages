# Maintainer: Alexey Pavlov <alexpux@gmail.com>

_realname=glib2
pkgname="${MINGW_PACKAGE_PREFIX}-${_realname}"
pkgver=2.43.1
pkgrel=1
url="http://www.gtk.org/"
arch=('any')
pkgdesc="Common C routines used by GTK+ 2.4 and other libs (mingw-w64)"
license=('LGPL')
options=('!debug' 'strip' 'staticlibs')
install=glib2-${CARCH}.install
depends=("${MINGW_PACKAGE_PREFIX}-gcc-libs"
        "${MINGW_PACKAGE_PREFIX}-gettext"
        "${MINGW_PACKAGE_PREFIX}-zlib"
        "${MINGW_PACKAGE_PREFIX}-libffi")
makedepends=("${MINGW_PACKAGE_PREFIX}-gcc" "${MINGW_PACKAGE_PREFIX}-python2")
source=("https://download.gnome.org/sources/glib/${pkgver%.*}/glib-$pkgver.tar.xz"
        0001-Use-CreateFile-on-Win32-to-make-sure-g_unlink-always.patch
        0003-g_abort.all.patch
        0004-glib-prefer-constructors-over-DllMain.patch
        0005-glib-send-log-messages-to-correct-stdout-and-stderr.patch
        0015-fix-stat.all.patch
        0017-glib-use-gnu-print-scanf.patch
        0021-use-64bit-stat-for-localfile-size-calc.all.patch
        0023-print-in-binary-more-for-testing.all.patch
        0024-return-actually-written-data-in-printf.all.patch
        0026-if-indextoname.patch)
sha256sums=('feccbf5982c200abd283a7980a8897c32cc7b246c281ed44636907168d99cb79'
            '924858843793d71646890927a03b5a71f5b4b55dda6a0ff26c9112c0e5e8fc36'
            '6df7b4e00aa7ded9e2e9e729dd1fb3f78e11c5936b316ebf9cf102c9043756f4'
            '7d1096d4a799df63939ded9437bcd39ad13ab71ba213ef7c83abd6ebf4efaf6e'
            '4e636326f39261dc359784853bd61a897f8bb2d438fffc256ae60bb580799f16'
            '9ba68777f48816cbee154084c10d0d577a021ef406606d5c2d230761cf82b66b'
            'ada29ac98e59bc35d60a0029473441d8b8f26868610b93b36c608db538ccbba7'
            'ad13c9aaa3d2ca7b1e3dcfd6d0c8043fa4fe45d8abb3659dbbc1fcbc412c10c7'
            '2fd972b55bc9725479e2c62085940e1986f4c3dfde74a791e8e990f57bbe2919'
            '497d057e79943df233ca304c389f462d3c4bf55ab3267e94dbef892b79fe3274'
            'fddad593d0c76bb00dfbc8858331431c9a5c560fa643cd6b00fa2678960470de')
prepare() {
  cd "$srcdir/glib-$pkgver"

  patch -Np1 -i "$srcdir/0001-Use-CreateFile-on-Win32-to-make-sure-g_unlink-always.patch"
  patch -Np1 -i "$srcdir/0003-g_abort.all.patch"
  patch -Np1 -i "$srcdir/0004-glib-prefer-constructors-over-DllMain.patch"
  patch -Np0 -i "$srcdir/0005-glib-send-log-messages-to-correct-stdout-and-stderr.patch"
  patch -Np1 -i "$srcdir/0015-fix-stat.all.patch"
  patch -Np1 -i "$srcdir/0017-glib-use-gnu-print-scanf.patch"
  patch -Np1 -i "$srcdir/0021-use-64bit-stat-for-localfile-size-calc.all.patch"
  patch -Np1 -i "$srcdir/0023-print-in-binary-more-for-testing.all.patch"
  patch -Np1 -i "$srcdir/0024-return-actually-written-data-in-printf.all.patch"

  NOCONFIGURE=1 ./autogen.sh

  patch -Np1 -i "$srcdir/0026-if-indextoname.patch"
}

build()
{
  cd "$srcdir/glib-$pkgver"

  export MSYS2_ARG_CONV_EXCL="-//OASIS//DTD"
  msg "Build shared version"
  mkdir $srcdir/build-${CARCH}-shared && cd $srcdir/build-${CARCH}-shared
  ../glib-$pkgver/configure \
    --prefix=${MINGW_PREFIX} \
    --build=${MINGW_CHOST} \
    --host=${MINGW_CHOST} \
    --target=${MINGW_CHOST} \
    --disable-static --enable-shared \
    --disable-libelf \
    --with-python=${MINGW_PREFIX}/bin/python.exe \
    --with-threads=posix \
    --with-xml-catalog=${MINGW_PREFIX}/etc/xml/catalog
  make

  msg "Build static version"
  mkdir $srcdir/build-${CARCH}-static && cd $srcdir/build-${CARCH}-static
  ../glib-$pkgver/configure \
    --prefix=${MINGW_PREFIX} \
    --build=${MINGW_CHOST} \
    --host=${MINGW_CHOST} \
    --target=${MINGW_CHOST} \
    --disable-shared --enable-static \
    --disable-libelf \
    --with-python=${MINGW_PREFIX}/bin/python.exe \
    --with-threads=posix \
    --with-xml-catalog=${MINGW_PREFIX}/etc/xml/catalog
  make
}

package() {
  cd "$srcdir/build-${CARCH}-static"
  make DESTDIR="$pkgdir/static" install

  cd "$srcdir/build-${CARCH}-shared"
  make DESTDIR="$pkgdir" install
  
  mv "$pkgdir/static/${MINGW_PREFIX}/lib/"*.a "$pkgdir/${MINGW_PREFIX}/lib/"

  rm -rf "${pkgdir}/static"

  rm -f "${pkgdir}${MINGW_PREFIX}"/lib/charset.alias
  #rm -rf "${pkgdir}${MINGW_PREFIX}"/{share/{gtk-doc,man,bash-completion,gdb},lib/charset.alias,etc}
  rm "${pkgdir}${MINGW_PREFIX}/lib/"*.def

  #rm -f "${pkgdir}${MINGW_PREFIX}/bin/gdbus-codegen"
  #rm -rf "${pkgdir}${MINGW_PREFIX}/lib/gdbus-2.0"
}
